from flask import Flask, render_template
from kjk_maths import app

@app.route('/')
@app.route('/home')
def root():
    return render_template('home.html')

@app.route('/game')
def game():
    return render_template('game.html')
